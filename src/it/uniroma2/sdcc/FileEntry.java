package it.uniroma2.sdcc;

public class FileEntry {
	
	String recordingID;
	String name;
	String duration; // minutes
	String dimension; // bytes
	String url;
	String date;
	float latitude;
	float longitude;
	
	public FileEntry(String recordingID, String name, String duration,
			String dimension, String url, String date,
			float latitude, float longitude) {
		if (recordingID == null || duration == null || dimension == null || url == null || date == null) {
			throw new NullPointerException("On FileEntry construction, null arguments.");
		}
		
		this.recordingID = recordingID;
		this.name = name;
		this.duration = duration;
		this.dimension = dimension;
		this.url = url;
		this.date = date;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getID() {
		return recordingID;
	}

	public String getName() {
		return name;
	}

	public String getDuration() {
		return duration;
	}
	
	public String getDimension() {
		return dimension;
	}

	public String getUrl() {
		return url;
	}
	
	public String getDate() {
		return date;
	}
	
	public float getLatitude() {
		return latitude;
	}
	
	public float getLongitude() {
		return longitude;
	}
}
