package it.uniroma2.sdcc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.json.JSONObject;

import it.uniroma2.sdcc.gui.RoomActivity;
import it.uniroma2.sdcc.gui.SplashScreen;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class RecorderService extends Service {
	
	public static final int CODEC_CHANNELS = 1;
	public static final int CODEC_SAMPLERATE = 44100;
	public static final int CODEC_BITRATE = 96000;

	public static final String CRED_KEY_USERNAME = "username";
	public static final String CRED_KEY_TOKEN = "token";
	public static final String CRED_KEY_ROOMID = "roomId";

	public class RecorderBinder extends Binder {
		public RecorderService getService() {
			return RecorderService.this;
		}
	}
	
	static final String TAG = "RecorderService";

	CodecInfo codecInfo = new AAC_CodecInfo(CODEC_CHANNELS, CODEC_SAMPLERATE, CODEC_BITRATE);

	RecorderThread recorder;
	Thread encoder;
	NetworkThread worker;
	
	boolean started;

	Bundle extras;

	private AudioChunk prepareCredentials(String username, String token, String roomID)
	{
		ByteArrayOutputStream outputStream;
		DataOutputStream dataStream;

		HashMap<String, String> m = new HashMap<>();

		m.put(CRED_KEY_USERNAME, username);
		m.put(CRED_KEY_TOKEN, token);
		m.put(CRED_KEY_ROOMID, roomID);

		JSONObject jsonCred = new JSONObject(m);

		String json = jsonCred.toString();

		outputStream = new ByteArrayOutputStream(4 + json.length());
		dataStream = new DataOutputStream(outputStream);

		try {
			dataStream.writeInt(json.length());
			dataStream.writeBytes(json);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new AudioChunk(outputStream.toByteArray(), 4+json.length());
	}
		
	@Override
	public IBinder onBind(Intent arg0) {

		extras = arg0.getExtras();

		return new RecorderBinder();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		Log.i(TAG, "Created.");
	}
	
	public void start() {
		
		if (started) {
			Log.w(TAG, "Already started");
			return;
		}
		
		/*
		 * Parsing of user and room data
		 */

		String username = extras.getString(SplashScreen.SPLASH_NAME);
		String token = extras.getString(SplashScreen.SPLASH_TOKEN);
		RoomEntry room = (RoomEntry) extras.getSerializable(RoomActivity.ROOM_OBJECT);

		Log.d(TAG, "Server Host: " + room.getUrl());

		/*
		 * Here we init the processing block chain
		 * 
		 * |^^^^^^^^^^|    |^^^^^^^^^|    |^^^^^^^^^|
		 * | Recorder | => | Encoder | => | Network |
		 * |__________|    |_________|    |_________|
		 * 
		 * The captured audio will cross different processing threads
		 * using producer/consumer queue mechanism.
		 * 
		 * Initialization and thread start follows REVERSE CHAIN ORDER:
		 * 	1) Start Network
		 *  2) Start Encoder
		 *  3) Start Recorder
		 * 
		 * Termination MUST BE performed in CHAIN ORDER:
		 * 	1) Stop Recorder
		 *  2) Stop Encoder
		 *  3) Stop Network
		 */
		
		AudioChunk credentials = prepareCredentials(username, token, room.getID());

		worker = new NetworkThread(room.getUrl(), credentials);
		// encoder = new EncoderThread(worker.getInputQueue());
		encoder = new EncoderOutputThread(codecInfo, worker.getInputQueue());
		recorder = new RecorderThread(codecInfo);

		worker.start();
		encoder.start();
		recorder.start();
		
		started = true;
		
		Notification noti = new Notification.Builder(this)
        .setContentTitle("SDCC Client Demo")
        .setContentText("Record in progress...")
        .setSmallIcon(R.drawable.ic_launcher)
        .build();

		startForeground(1, noti);
	}
	
	public void stop() {
		
		if (!started) {
			Log.w(TAG, "Already stopped");
			return;
		}
		
		
		try {
			recorder.stopRecording();
			recorder.join();
			
			// encoder.interrupt();
			encoder.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		worker.disconnectStop();
		
		stopForeground(true);
		
		started = false;
	}
	
	public boolean isStarted() {
		return started;
	}
	
	@Override
	public void onDestroy() {
		
		Log.i(TAG, "onDestroy()");
		
		super.onDestroy();
	}
}
