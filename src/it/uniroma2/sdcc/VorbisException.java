package it.uniroma2.sdcc;

public class VorbisException extends Exception {

	private static final long serialVersionUID = 3333438097463826108L;
	
	public VorbisException(String message) {
		super(message);
	}
}
