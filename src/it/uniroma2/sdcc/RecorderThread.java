package it.uniroma2.sdcc;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

public class RecorderThread extends Thread {
	
	static final String TAG = "RecorderThread";
	
	public static final int SAMPLE_RATE_HZ = 44100;
	public static final int BATCH_BUFFER_SIZE = 65536;
	
	private AudioRecord recorder;
	
	private EncoderInput encoderInput;
	
	// private Queue<AudioChunk> outputQueue;
	
	private final int internalBufferSize;
	
	public RecorderThread(CodecInfo codecInfo) {
		
		encoderInput = new EncoderInput(codecInfo);
		
		/*if (outputQueue == null)
			throw new NullPointerException("outputQueue is null");
		
		this.outputQueue = outputQueue;*/
		
		internalBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE_HZ, 
				  								          AudioFormat.CHANNEL_IN_MONO,
				  								          AudioFormat.ENCODING_PCM_16BIT);

		Log.i(TAG, "onCreate() => min buffer size: " + internalBufferSize);

		// Log.i(TAG, "onCreate() => selected buffer size: " + bufferSize);

		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
								   SAMPLE_RATE_HZ,
								   AudioFormat.CHANNEL_IN_MONO,
								   AudioFormat.ENCODING_PCM_16BIT,
								   internalBufferSize);

		if (recorder.getState() != AudioRecord.STATE_INITIALIZED)
			throw new RuntimeException("Something went wrong in AudioRecorder creation");
	}
	
	@Override
	public void run() {
		
		// Buffer Initial Allocation
		
		byte[] batchBuffer = new byte[BATCH_BUFFER_SIZE];
		int batchOccupied = 0;
				
		recorder.startRecording();
		
		while (true) {
			int rb = recorder.read(batchBuffer, batchOccupied, Math.min(BATCH_BUFFER_SIZE-batchOccupied, internalBufferSize));
			
			if (rb < 0) {
				Log.i(TAG, "Recorder Loop interrupted");
				break;
			}
			
			batchOccupied += rb;
			
			if (batchOccupied >= BATCH_BUFFER_SIZE) {
				// flush
				
				if (batchOccupied != BATCH_BUFFER_SIZE)
					throw new RuntimeException("Batch buffer sizes does not match");
				
				// ENCODE
				
				encoderInput.put(batchBuffer, BATCH_BUFFER_SIZE);
				
				/* if (!outputQueue.offer(new AudioChunk(batchBuffer, BATCH_BUFFER_SIZE))) {
					Log.w("RecorderThread", "Output Queue full");
				}*/
				
				// realloc
				
				// batchBuffer = new byte[BATCH_BUFFER_SIZE];
				batchOccupied = 0;
			}
		}
		
		encoderInput.put(null, 0); // END-OF-STREAM
		
		Log.i(TAG, "Thread exit");
	}
	
	public void stopRecording() {
		recorder.stop();
	}
	
	public void release() {
		recorder.release();
		recorder = null;
	}
	
	@Override
	protected void finalize() throws Throwable {
		Log.i(TAG, "Finalized");
		super.finalize();
	}
}
