package it.uniroma2.sdcc;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import android.util.Log;

public class NetworkThread extends Thread {
	
	public static final String TAG = "NetworkThread";
	public static final int CONNECT_SLEEP_SECONDS = 5;
	public static final int CONNECT_MAX_RETRIES = 4;
	public static final int KEEP_ALIVE_TIMEOUT_MS = 10000;
	
	public static volatile boolean networkGood;

	private Socket sock;
	
	private BlockingQueue<AudioChunk> queue;
	
	private static int totalBytesSent;

	private AudioChunk credentials;
	
	private String host;

	private InetSocketAddress hostAddress;

	public NetworkThread(String host, AudioChunk cred) {
		
		queue = new ArrayBlockingQueue<AudioChunk>(1024, true);
		
		// sock = new Socket();

		this.host = host;
		
		credentials = cred;

		networkGood = false;

		totalBytesSent = 0;
	}

	public static int getBytesSent() {
		return totalBytesSent;
	}

	private void tryConnect() throws IOException {
		
		hostAddress = new InetSocketAddress(host, 9999);
		
		sock = new Socket();

		sock.connect(hostAddress);

		Log.i(TAG, "Connected.");

		sock.getOutputStream().write(credentials.getBuffer(), 0, credentials.getSize());
	}
	
	private void sendOut(final AudioChunk job) {
		try
		{
			sock.getOutputStream().write(job.getBuffer(), 0, job.getSize());
			totalBytesSent += job.getSize();
		}
		catch (IOException e) {
			networkGood = false;
		}
	}

	@Override
	public void run() {
		
		Log.i(TAG, "Thread start");
				
		AudioChunk job = null;
		
		totalBytesSent = 0;
		
		// int retries = 0;
		int timeout = CONNECT_SLEEP_SECONDS*1000;

		while (true) {
			try
			{
				if (!networkGood) {
					tryConnect();
					queue.clear();
					// retries = 0;
					// timeout = CONNECT_SLEEP_SECONDS*1000;
					networkGood = true;
					new KeepAliveThread(sock, KEEP_ALIVE_TIMEOUT_MS).start();
				}

				job = queue.take();
			} catch (IOException e) {

				/*if (retries > CONNECT_MAX_RETRIES) {
					Log.w(TAG, "Max connection retries reached, stop.");
					break;
				}

				retries++; */

				Log.e(TAG, "Unable to connect, retrying in " + timeout + " sec...");
				try {
					Thread.sleep(timeout);
					// timeout *= 2; // Doubling timeout value
					continue;
				} catch (InterruptedException e1) {
					Log.i(TAG, "Network Loop interrupted (while sleeping)");
					break;
				}
			}
			catch (InterruptedException e) {
				Log.i(TAG, "Network Loop interrupted");
				break;
			}
			
			sendOut(job);
		}
		
		Log.i(TAG, "Flush queue...");
		
		while (!queue.isEmpty()) {
			job = queue.poll();
			
			if (networkGood) {
				sendOut(job);
			}
		}
		
		Log.i(TAG, "Elements in queue: " + queue.size());
		Log.i(TAG, "TotalBytesSent: " + totalBytesSent);
		Log.i(TAG, "Thread exit");
	}

	
	public void disconnectStop() {
		
		interrupt();
		
		try {
			join();
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Queue<AudioChunk> getInputQueue() {
		return queue;
	}
}
