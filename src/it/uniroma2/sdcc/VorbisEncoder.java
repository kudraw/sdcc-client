package it.uniroma2.sdcc;

import java.util.Queue;
import java.util.Random;

import org.xiph.libogg.ogg_packet;
import org.xiph.libogg.ogg_page;
import org.xiph.libogg.ogg_stream_state;
import org.xiph.libvorbis.vorbis_block;
import org.xiph.libvorbis.vorbis_comment;
import org.xiph.libvorbis.vorbis_dsp_state;
import org.xiph.libvorbis.vorbis_info;
import org.xiph.libvorbis.vorbisenc;

public class VorbisEncoder implements Encoder {
	
	final static String TAG = "VorbisEncoder";
	
	private ogg_stream_state oss;
	private ogg_page headerPage;
	
	private ogg_page currentPage;
	private ogg_packet currentPacket;
	
	private vorbis_dsp_state vd;
	private vorbis_block vb;

	private boolean endOfStream;
	
	/**
	 * This function will initialize vorbis encoder given a set of user defined parameters
	 * 
	 * @param channels - Number of channels
	 * @param sampleRate - Sample rate
	 * @param quality - A number from 0.0 (low quality) to 1 (highest quality)
	 * @return a vorbis encoder
	 * @throws VorbisException
	 */
	public static VorbisEncoder getEncoder(int channels, int sampleRate, float quality) throws VorbisException {
		vorbisenc encoder;
		vorbis_info vi;
		vorbis_dsp_state vd;
		
		vi = new vorbis_info();
		
		encoder = new vorbisenc();
		
		if (!encoder.vorbis_encode_init_vbr(vi, channels, sampleRate, quality)) {
			throw new VorbisException("Failed to init encoder");
		}
		
		vd = new vorbis_dsp_state();

		if ( !vd.vorbis_analysis_init(vi) ) {
			throw new VorbisException("Failed to init vorbis_dsp_state");
		}
		
		return new VorbisEncoder(vd);
	}
	
	private void generateHeader(Queue<AudioChunk> outputQueue) {
		
		if (headerPage != null) return; // already generated
		
		headerPage = new ogg_page();

		ogg_packet header = new ogg_packet();
		ogg_packet header_comm = new ogg_packet();
		ogg_packet header_code = new ogg_packet();
		
		vorbis_comment vc = new vorbis_comment();
		vc.vorbis_comment_add_tag( "ENCODER", "SDCC Client Encoder" );
		
		vd.vorbis_analysis_headerout( vc, header, header_comm, header_code );
		
		oss.ogg_stream_packetin(header);
		oss.ogg_stream_packetin(header_comm);
		oss.ogg_stream_packetin(header_code);
		
		while (oss.ogg_stream_flush(headerPage)) {
			outputQueue.offer(new AudioChunk(headerPage.header, headerPage.header_len));
			outputQueue.offer(new AudioChunk(headerPage.body, headerPage.body_len));
		}
	}

	private VorbisEncoder(vorbis_dsp_state vd) {
		this.vd = vd;
		this.oss = new ogg_stream_state(new Random().nextInt(256));
		this.headerPage = null;
		
		this.vb = new vorbis_block(vd);
		
		this.currentPacket = new ogg_packet();
		this.currentPage = new ogg_page();
		
		this.endOfStream = false;
	}
	
	@Override
	public void encode(byte[] input, int len, Queue<AudioChunk> outputQueue) {
		// NOTE: here we assume input: WAV 1 channel 16 bit 44100Hz
		
		generateHeader(outputQueue);
		
		final int samples;
		
		if (input == null) // signal to terminate processing
		{
			samples = 0;
		}
		else {
			samples = len/2;
			
			float[][] buffer = vd.vorbis_analysis_buffer(samples);
			
			// short[] input_shorts = new short[samples];
			
			// ByteBuffer.wrap(input).asShortBuffer().get(input_shorts);
			
			for (int i=0; i < samples; i++) {
				
				float current = ((input[i*2+1] << 8) | 0x00ff & (int)input[i*2])/32768.f;
				
				buffer[0][vd.pcm_current+i] = current;
				buffer[1][vd.pcm_current+i] = current;
			}
		}
		
		vd.vorbis_analysis_wrote(samples);
		
		while ( vb.vorbis_analysis_blockout(vd) ) {
			vb.vorbis_analysis(null);
			vb.vorbis_bitrate_addblock();
			
			while (vd.vorbis_bitrate_flushpacket(currentPacket)) {
				
				oss.ogg_stream_packetin(currentPacket);
				
				while (!endOfStream && oss.ogg_stream_pageout(currentPage)) {
					
					outputQueue.offer(new AudioChunk(currentPage.header, currentPage.header_len));
					outputQueue.offer(new AudioChunk(currentPage.body, currentPage.body_len));
					
					if (currentPage.ogg_page_eos() > 0) {
						endOfStream = true;
					}
				}
			}
		}
	}
}
