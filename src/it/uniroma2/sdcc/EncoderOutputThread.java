package it.uniroma2.sdcc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Queue;

import android.media.MediaCodec;
import android.os.Environment;
import android.util.Log;

/**
 * EncoderOutputThread fetch and transfer data from the Android Encoder Output
 * to a consumer, through an output queue.
 * 
 * Once started, an End-Of-Stream (EOS) will automatically signal the end of process, and the
 * thread automatically stops.
 * 
 * @author CoDeP Team
 *
 */
public class EncoderOutputThread extends Thread {
	
	public final String TAG = "EncoderOutputThread";
	
	private CodecInfo info;
	private Queue<AudioChunk> outputQueue;
	private boolean running;

	/**
	 * Constructor
	 * 
	 * @param info - A CodecInfo object that describes the particular Codec implementation.
	 * @param outputQueue - The consumer queue that receives the processed data for further processing.
	 */
	public EncoderOutputThread(CodecInfo info, Queue<AudioChunk> outputQueue) {
		this.info = info;
		this.outputQueue = outputQueue;
		this.running = false;
	}
	
	
	@Override
	public void run() {
		
		running = true;

		SimpleDateFormat frmt = new SimpleDateFormat("yyyy_MM_dd_HHmmss", Locale.getDefault());
		File podcasts = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS);
		BufferedOutputStream bos = null;

		try {
			File recordingFile = new File(podcasts, frmt.format(new Date())+".aac");
			Log.i(TAG, "Creating file: " + recordingFile.toString());
			bos = new BufferedOutputStream(new FileOutputStream(recordingFile));
		} catch (IOException e) {
			Log.e(TAG, "Unable to create recording file.");
		}

		while (running) {
			MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
	        int outputBufferIndex = info.codec.dequeueOutputBuffer(bufferInfo, 0); // -1 => will block waiting for output (?)
	        
	        while (outputBufferIndex >= 0) {
	        	
	        	if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
	        		Log.i(TAG, "End-Of-Stream Read");
	        		
	        		running = false;
	        	}
	        	
	        	int length = bufferInfo.size;
	            int totalLength = length + info.getHeaderSize();
	        	
	            ByteBuffer outputBuffer = info.outputBuffers[outputBufferIndex];
	            
	            // This will copy the buffer into an ordinary byte array, can be optimized
	            // without copying the buffer and postpone the releaseOutputBuffer after send ;).
	            byte[] outData = new byte[totalLength];
	            outputBuffer.position(bufferInfo.offset);
	            outputBuffer.get(outData, info.getHeaderSize(), length);
	            
	            // Now we put the header
	            info.fillHeader(outData, totalLength);

	            if (NetworkThread.networkGood) outputQueue.add(new AudioChunk(outData, outData.length));

	            info.codec.releaseOutputBuffer(outputBufferIndex, false);
	            
	            if (bos != null) {
					try {
						bos.write(outData);
					} catch (IOException e) {
						Log.e(TAG, "Error on file writing, no more file writes will be attempted.");
						try {
							bos.close();
						} catch (IOException e1) {
						}
						bos = null;
					}
	            }

	            if (!running) {
	            	Log.i(TAG, "Encoder Loop Interrupted");
	            	break;
	            }
	            
	            outputBufferIndex = info.codec.dequeueOutputBuffer(bufferInfo, 0);
	        }
		}

		if (bos != null) {
			try {
				bos.close();
			} catch (IOException e) {
				Log.w(TAG, "Error while closing file.");
			}
		}
		
		Log.i(TAG, "Thread exit");
		
		info.codec.release();
	}
	
	void terminate() {
		info.codec.stop();
		running = false;
	}
}
