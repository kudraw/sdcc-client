package it.uniroma2.sdcc;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class StatsThread extends Thread {
	
	public static final String TAG = "StatsThread";
	
	private Handler handler;
	
	private int bytesBefore;

	public StatsThread(Handler h) {
		handler = h;
		bytesBefore = 0;
	}
	
	@Override
	public void run() {
		
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Log.i(TAG, "Interrupted");
				break;
			}
			
			int have_net = 0;
			int current_bytes = 0;
			int delta_bytes = 0;
			
			if (NetworkThread.networkGood) {
				have_net = 1;
				current_bytes = NetworkThread.getBytesSent();
				delta_bytes = current_bytes - bytesBefore;
				bytesBefore = current_bytes;
			}
			
			Message msg = Message.obtain(handler, 0, have_net, delta_bytes);
			
			handler.sendMessage(msg);
		}
	}
}
