package it.uniroma2.sdcc;

import android.media.MediaCodecInfo;
import android.media.MediaFormat;

/**
 * An AAC CodecInfo implementation.
 * 
 * This class wraps Android Encoder AAC LC configuration, and provides also
 * an ADTS header (see http://wiki.multimedia.cx/index.php?title=ADTS).
 * 
 * @author CoDeP Team
 *
 */
public class AAC_CodecInfo extends CodecInfo {
	
	public static final String AUDIO_MIME_TYPE = "audio/mp4a-latm";
	public static final int AAC_ADTS_HEADER_SIZE = 7;

	public AAC_CodecInfo(int channels, int sampleRate, int bitRate) {
		super(AUDIO_MIME_TYPE, channels, sampleRate, bitRate);
	}

	@Override
	public void configureFormat(MediaFormat audioFormat) {
		audioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
	}

	/**
	 *  Add ADTS header at the beginning of each and every AAC packet.
	 *  This is needed as MediaCodec encoder generates a packet of raw
	 *  AAC data.
	 *  
	 *  @param packet - The raw AAC Frame with enough room for ADTS header at begin
	 *  @param packetLen - The whole packet len including header
	 **/
	@Override
	public void fillHeader(byte[] packet, int packetLen) {

		// FIXME: The frequency index should not be hardcoded
	    final int freqIdx = 4;  //44.1KHz

	    // fill in ADTS data
	    packet[0] = (byte)0xFF;
	    packet[1] = (byte)0xF9;
	    packet[2] = (byte)(((MediaCodecInfo.CodecProfileLevel.AACObjectLC-1)<<6) + (freqIdx<<2) +(channels>>2));
	    packet[3] = (byte)(((channels&3)<<6) + (packetLen>>11));
	    packet[4] = (byte)((packetLen&0x7FF) >> 3);
	    packet[5] = (byte)(((packetLen&7)<<5) + 0x1F);
	    packet[6] = (byte)0xFC;
	}

	/**
	 * Specify the size of ADTS header in bytes.
	 * 
	 * Usually beetween 7 (no-CRC) and 9 (with CRC) bytes.
	 * 
	 * @return The size of ADTS header 
	 */
	@Override
	public int getHeaderSize() {
		return AAC_ADTS_HEADER_SIZE;
	}
}
