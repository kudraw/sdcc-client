package it.uniroma2.sdcc;

import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaFormat;

/**
 * This class will wrap Android MediaCodec class to handle
 * encoding of audio stream.
 * 
 * The class perform all the major initializations, but is abstract
 * and need to be extended.
 * 
 * The sub-classes must implement configureFormat,
 * fillHeader and getHeaderSize functions (if needed).
 * 
 * @author CoDeP Team
 *
 */
public abstract class CodecInfo {

	public final MediaCodec codec;
	
	public final int channels;
	public final int sampleRate;
	public final int bitRate;
	
	public final ByteBuffer[] inputBuffers;
	public final ByteBuffer[] outputBuffers;
	
	/**
	 * Configure format will be called in the CodecInfo constructor, and
	 * should be implemented by sub-classes in order
	 * to push to configure MediaFormat object with specific data for the implementing
	 * Codec.
	 * 
	 * @param audioFormat - The MediaFormat object
	 */
	public abstract void configureFormat(MediaFormat audioFormat);
	
	/**
	 * This function should be implemented if an header should be appended to each frame.
	 * 
	 * Note that you must also implement getHeaderSize().
	 *
	 * @param input - The input buffer, should have enough room to contain header
	 * @param len - The total packet len, including the header
	 */
	public abstract void fillHeader(byte[] input, int len);
	
	/**
	 * This function should return the number of bytes of header.
	 * 
	 * Note: if you return 0, and fillHeader(..) function body is empty, the system will not
	 * process header, and outputs the raw data packet.
	 * 
	 * @return Header size in bytes 
	 */
	public abstract int getHeaderSize();
	
	/**
	 * Construct a CodecInfo object.
	 * 
	 * Note that this class is abstract, so you should override the class and a provide a constructor,
	 * calling this with super method.
	 * 
	 * @param mimeType - An Android valid mimetype for encoding.
	 * @param channels - Number of audio channels
	 * @param sampleRate - Sample rate in Hz (i.e. 44100)
	 * @param bitRate - Desired Bit rate of the encoded stream
	 */
	public CodecInfo(String mimeType, int channels, int sampleRate, int bitRate) {
		this.channels = channels;
		this.sampleRate = sampleRate;
		this.bitRate = bitRate;
		
		codec = MediaCodec.createEncoderByType(mimeType);
		
		MediaFormat audioFormat = new MediaFormat();
		
		audioFormat.setString(MediaFormat.KEY_MIME, mimeType);
		audioFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, RecorderThread.BATCH_BUFFER_SIZE);
		audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
		
		audioFormat.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channels);
		audioFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, sampleRate);
		
		configureFormat(audioFormat);
		
		codec.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
		
		codec.start();
		
		inputBuffers = codec.getInputBuffers();
        outputBuffers = codec.getOutputBuffers();
	}
}
