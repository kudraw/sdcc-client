package it.uniroma2.sdcc.nethelpers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

public class REST {
	private static REST instance = null;
	private String token;
	
	private REST() {
		token = "";
	}
	
	public static REST getInstance() {
		if (instance == null)
			instance = new REST();
		return instance;
	}
	
	public void destroy() {
		instance = null;
	}
	
	public void setToken(String t) {
		if (t != null)
			token = t;
	}
	
	/* ROOMS REST INTERFACES */
	
	public String getRoomList() throws RESTException {
		String url = URLs.REST_SERVER + URLs.ROOMS_LIST;
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.getRoomList", "Sending GET request to " + url);
		
		try {
			connection = getConnection("GET", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	public String getRoomList(float latitude, float longitude, int radius) throws RESTException {
		String url = URLs.REST_SERVER + URLs.ROOMS_LIST + 
				"?lat=" + latitude +
				"&lon=" + longitude +
				"&radius=" + radius;
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.getRoomList", "Sending GET request to " + url);
		
		try {
			connection = getConnection("GET", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	public String createRoom(String message) throws RESTException {
		String url = URLs.REST_SERVER + URLs.ROOMS_LIST;
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.createRoom", "Sending POST request to " + url);

		try {
			connection = getConnection("POST", url);
			
			writeString(connection, message);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	public String getRoomDetails(String roomID) throws RESTException {
		String url = URLs.REST_SERVER + URLs.ROOMS_LIST + roomID + "/";
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.getRoomDetails", "Sending GET request to " + url);
		
		try {
			connection = getConnection("GET", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	/* RECORDINGS REST INTERFACES */
	
	public String getUserFileList() throws RESTException {
		String url = URLs.REST_SERVER + URLs.RECORDINGS_LIST;
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.getUserFileList", "Sending GET request to " + url);
		
		try {
			connection = getConnection("GET", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	public String getFileDetails(String recordingID) throws RESTException {
		String url = URLs.REST_SERVER + URLs.RECORDINGS_LIST + recordingID + "/";
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.getFileDetails", "Sending GET request to " + url);
		
		try {
			connection = getConnection("GET", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
			
			return readString(connection);
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	public void deleteFile(String recordingID) throws RESTException {
		String url = URLs.REST_SERVER + URLs.RECORDINGS_LIST + recordingID + "/";
		HttpURLConnection connection;
		int responseCode;
		
		Log.d("REST.deleteFile", "Sending DELETE request to " + url);
		
		try {
			connection = getConnection("DELETE", url);
			
			responseCode = connection.getResponseCode();
			if (responseCode < 200 || responseCode > 299) {
				throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
			}
		} catch (IOException e) {
			throw new RESTException(e.getMessage());
		}
	}
	
	private HttpURLConnection getConnection(String method, String url) throws IOException {
		URL urlObj;
		HttpURLConnection connection;
		
		urlObj = new URL(url);
		
		connection = (HttpURLConnection) urlObj.openConnection();
		connection.setRequestMethod(method);
		
		if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
		}

		connection.setDoInput(true);
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", "Token " + token);
		
		return connection;
	}
	
	private String readString(HttpURLConnection connection) throws IOException {
		InputStream content = connection.getInputStream();
		BufferedReader in = new BufferedReader (new InputStreamReader(content));
		String line;
		String string = "";
		while ((line = in.readLine()) != null) {
			string += line+"\n";
		}
		content.close();
		in.close();
		
		Log.d("REST.readString", "Server body response: " + string);
		
		return string;
	}
	
	private void writeString(HttpURLConnection connection, String message) throws IOException {
		
		Log.d("REST.writeString", "Client body request: " + message);
		
		OutputStream os = connection.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);
		dos.write(message.getBytes("UTF-8"));
		dos.write('\n');
		dos.flush();
		dos.close();
		os.close();
	}
	
}
