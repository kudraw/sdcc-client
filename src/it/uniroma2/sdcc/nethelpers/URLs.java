package it.uniroma2.sdcc.nethelpers;

public class URLs {
	public static final String REST_SERVER = "http://rest.codep.quantumb.it";
	
	public static final String TOKEN_URL = "/rest/token/get/";
	
	/* Rooms */
	public static final String ROOMS_LIST = "/rest/rooms/";
	
	/* Recordings */
	public static final String RECORDINGS_LIST = "/rest/recordings/";
}
