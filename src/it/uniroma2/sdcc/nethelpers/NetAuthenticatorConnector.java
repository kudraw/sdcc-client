package it.uniroma2.sdcc.nethelpers;

import it.uniroma2.sdcc.authenticator.AuthenticationException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class NetAuthenticatorConnector {
	
	public static String userSignIn(String user, String password) throws IOException, JSONException, AuthenticationException {
		URL url;
		HttpURLConnection connection;
		
		url = new URL(URLs.REST_SERVER + URLs.TOKEN_URL);
		
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		
		connection.setDoOutput(true);
		connection.setDoInput(true);

		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Content-Type", "application/json");
		
		JSONObject json = new JSONObject();
		json.put("username", user);
		json.put("password", password);
		
		OutputStream os = connection.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);
		dos.write(json.toString().getBytes("UTF-8"));
		dos.write('\n');
		dos.flush();
		dos.close();
		os.close();
		
		// Check response code
		
		int resp = connection.getResponseCode();
		
		if (resp < 200 || resp >= 300) {
			Log.w("authentication backend", "Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
			throw new AuthenticationException("Cannot authenticate the user. Server error " + resp + ": " + connection.getResponseMessage());
		}
		
		InputStream content = connection.getInputStream();
		BufferedReader in = new BufferedReader (new InputStreamReader(content));
		String line;
		String stringJson = "";
		while ((line = in.readLine()) != null) {
			stringJson += line+"\n";
		}
		content.close();
		in.close();
		
		Log.d("authentication backend", "Response message: " + stringJson);
		
		JSONObject respJson = new JSONObject(stringJson);
		String token = respJson.getString("token");
		Log.d("authentication backend", "Read token \"" + token + "\" for user \"" + user + "\"");
		
		return token; 
	}
}
