package it.uniroma2.sdcc;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import android.util.Log;

public class EncoderThread extends Thread {
	
	public static String TAG = "EncoderThread";

	private Encoder enc;
	
	private BlockingQueue<AudioChunk> jobQueue;
	private Queue<AudioChunk> outputQueue;
	
	public EncoderThread(Queue<AudioChunk> outputQueue) {
		
		if (outputQueue == null) {
			throw new NullPointerException("Output queue cannot be null");
		}
		
		this.outputQueue = outputQueue;
		
		try {
			enc = VorbisEncoder.getEncoder(2, 44100, 0.05f);
		} catch (VorbisException e) {
			e.printStackTrace();
		}
		
		jobQueue = new ArrayBlockingQueue<AudioChunk>(1024, true);
	}
	
	private void sendOut(AudioChunk job) {
		enc.encode(job.getBuffer(), job.getSize(), outputQueue);
	}
	
	@Override
	public void run() {
		
		Log.i(TAG, "Thread start");
				
		AudioChunk job;
		
		while (true) {
			try {
				job = jobQueue.take();
			} catch (InterruptedException e) {
				Log.i(TAG, "Encoder Loop interrupted");
				break;
			}
				
			sendOut(job);
		}
		
		Log.i(TAG, "Flushing queue...");
		
		while (!jobQueue.isEmpty()) {
			job = jobQueue.poll();
			
			sendOut(job);
		}
		
		enc.encode(null, 0, outputQueue); // Mark termination of stream
		
		Log.i(TAG, "Elements in queue: " + jobQueue.size());
		Log.i(TAG, "Thread exit");
	}
	
	public Queue<AudioChunk> getInputQueue() {
		return jobQueue;
	}
}
