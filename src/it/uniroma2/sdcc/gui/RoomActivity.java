package it.uniroma2.sdcc.gui;

import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.RoomEntry;
import it.uniroma2.sdcc.nethelpers.REST;
import it.uniroma2.sdcc.nethelpers.RESTException;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class RoomActivity extends Activity {
	private static final int RADIUS = 1000; /* meters */
	public static final String ROOM_OBJECT = "ROOM_OBJECT";

	private String username = null;
	private String token = null;
	
	private ListView roomsList;
	private TextView emptyList;
	private ProgressBar waitBar;
	private RoomsAdapter roomsAdapter;
	
	private LocationManager locationManager;
	private RoomsUpdater roomsUpdater;
	private RoomsUpdaterCoarse roomsUpdaterCoarse;
	private Location location;
	private static final Lock lock = new ReentrantLock();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Log.d("RoomActivity.onCreate", "Activity created");

		roomsList = (ListView) findViewById(R.id.list);
		waitBar = (ProgressBar) findViewById(R.id.wait_for_file_list);
		emptyList = (TextView) findViewById(R.id.empty_list_message);
		
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		
		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			username = extras.getString(SplashScreen.SPLASH_NAME);
			token = extras.getString(SplashScreen.SPLASH_TOKEN);
		}
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		Log.d("RoomActivity.onStart", "Activity started");
		
		// Set initial location
		try {
			lock.lock();
			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (location != null)
				Log.i("RoomActivity.onStart", "Initial location: Lat=" + location.getLatitude() + " Lon=" + location.getLongitude()
						+ " Accuracy=" + location.getAccuracy() + " Provider: " + location.getProvider());
		} finally {
			lock.unlock();
		}
		
		roomsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				RoomEntry entry = roomsAdapter.getItem(position);
				
				new AsyncTask<RoomEntry, Void, RoomEntry>() {

					@Override
					protected RoomEntry doInBackground(RoomEntry... params) {
						RoomEntry e = params[0];
						String roomID = e.getID();
						
						REST rest = REST.getInstance();
						try {
							
							String response = rest.getRoomDetails(roomID);
							JSONObject json = new JSONObject(response);
							e.setUrl(json.getString("stream_url"));
							
						} catch (RESTException | JSONException e1) {
							Log.e("RoomActivity.OnItemClickListener", "Error fetching room details: " + e1.getMessage());
							return null;
						}
						
						return e;
					}
					
					protected void onPostExecute(RoomEntry result) {
						if (result == null) {
							Toast.makeText(RoomActivity.this, RoomActivity.this.getText(R.string.toast_failed), Toast.LENGTH_SHORT).show();
							return;
						}
						
						// Start new activity
						Intent intent = new Intent(RoomActivity.this, RecordingActivity.class);
						intent.putExtra(SplashScreen.SPLASH_NAME, username);
						intent.putExtra(SplashScreen.SPLASH_TOKEN, token);
						intent.putExtra(ROOM_OBJECT, result);
						
						startActivity(intent);
						
					};
					
				}.execute(entry);
			}
		});
		
		// Start location updater services
		roomsUpdater = new RoomsUpdater();
		roomsUpdaterCoarse = new RoomsUpdaterCoarse();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000 /* ms */, 50 /* m */, roomsUpdater);
		
		// This listener is used just to update cached network position, used to retrieve fast position when update button is pressed
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000 /* ms */, 50 /* m */, roomsUpdaterCoarse);

		Toast.makeText(this, getString(R.string.toast_radius_info) + " " + RADIUS + " m", Toast.LENGTH_SHORT).show();
		
		updateRoomList();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		// Stop location updater services
		locationManager.removeUpdates(roomsUpdater);
		locationManager.removeUpdates(roomsUpdaterCoarse);
	}
	
	private void updateRoomList() {
		emptyList.setVisibility(View.INVISIBLE);
		roomsList.setVisibility(View.INVISIBLE);
		waitBar.setVisibility(View.VISIBLE);
		
		Log.d("MainActivity.updateRoomList", "Updating list view");
		
		// Check for a more fine location 
		Location l = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (l != null) {
			try {
				lock.lock();
				
				if (location == null || !location.hasAccuracy() || (l.hasAccuracy() && l.getAccuracy() < location.getAccuracy())) {
					location = l;
					Log.i("MainActivity.updateRoomList", "Updated location: Lat=" + location.getLatitude() + " Lon=" + location.getLongitude()
							+ " Accuracy=" + location.getAccuracy() + " Provider: " + location.getProvider());
				}
				
			} finally {
				lock.unlock();
			}
		}
		
		roomsAdapter = new RoomsAdapter(this);
		roomsList.setAdapter(roomsAdapter);
		
		try {
			lock.lock();
			
			// If location provider were disabled, stop update
			if (location == null) {
				Toast.makeText(this, getString(R.string.toast_no_location), Toast.LENGTH_SHORT).show();
				Log.w("RoomActivity.updateRoomList", "No location retrieved");
				
				waitBar.setVisibility(View.INVISIBLE);
				emptyList.setVisibility(View.VISIBLE);
				
				return; // The unlock statement is always called
			}
			
		} finally {
			lock.unlock();
		}
		
		new AsyncTask<Void, JSONObject, Boolean>() {

			@Override
			protected Boolean doInBackground(Void... params) {
				REST rest = REST.getInstance();

				// Get location
				boolean hasLocation = true;
				float accuracy = 0;
				double latitude = 0;
				double longitude = 0;
				String provider = "";
				
				try {
					lock.lock();
					
					if (location == null) {
						hasLocation = false;
					}
					else {
						accuracy = location.getAccuracy();
						latitude = location.getLatitude();
						longitude = location.getLongitude();
						provider = location.getProvider();
					}
					
				} finally {
					lock.unlock();
				}
				
				try {
					
					// Continue only if there is a known location
					if (hasLocation) {
						Log.i("RoomActivity.updateRoomList", "Retrieving rooms for position: Lat=" + latitude + " Lon=" + longitude
								+ " Accuracy=" + accuracy + " Provider: " + provider);
	
						String response = rest.getRoomList((float) latitude, (float) longitude, RADIUS);
						
						JSONArray res = new JSONArray(response);
						
						for(int i = 0; i < res.length(); i++) {
							JSONObject o = res.getJSONObject(i);
							publishProgress(o);
						}
					}
					else {
						Log.w("RoomActivity.updateRoomList", "No location retrieved");
					}
					
					return true;
					
				} catch (RESTException | JSONException e) {
					Log.e("RoomActivity.updateFileList.doInBackground", "Error: " + e.getMessage());
				}
				
				return false;
			}
			
			protected void onProgressUpdate(JSONObject... values) {
				JSONObject o = values[0];
				
				try {
					
					RoomEntry entry = new RoomEntry(o.getString("roomID"), o.getString("roomName"),
							o.getInt("distance"), o.getInt("participant"), null);
					
					roomsAdapter.add(entry);
					
				} catch (JSONException e) {
					Log.e("RoomActivity.updateFileList.onProgressUpdate", "JSONException: " + e.getMessage());
				}
			};
			
			protected void onPostExecute(Boolean result) {
				if (result == true) {
					waitBar.setVisibility(View.INVISIBLE);
					if (roomsAdapter.isEmpty())
						emptyList.setVisibility(View.VISIBLE);
					else
						roomsList.setVisibility(View.VISIBLE);
				}
				else {
					waitBar.setVisibility(View.INVISIBLE);
					Toast.makeText(RoomActivity.this, getString(R.string.toast_server_error), Toast.LENGTH_SHORT).show();
				}
			};
		}.execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rooms, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		Log.d("RoomActivity", "Action selected");
		
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_update_rooms) {
			updateRoomList();
			return true;
		}
		else if (id == R.id.action_add_room) {
			Location l;
			try {
				lock.lock();
				
				// If location provider were disabled, stop update
				if (location == null) {
					Toast.makeText(this, getString(R.string.toast_no_location), Toast.LENGTH_SHORT).show();
					
					return false; // The unlock statement is always called
				}
				
				l = location;
				
			} finally {
				lock.unlock();
			}
			
			AlertDialog.Builder builder = new AlertDialog.Builder(RoomActivity.this);
			builder.setView(RoomActivity.this.getLayoutInflater().inflate(R.layout.new_room_dialog, null))
					.setTitle(R.string.room_dialog_title)
					.setPositiveButton(android.R.string.ok, new CreateRoomListener(l))
					.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
			}).create().show();
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class RoomsUpdater implements LocationListener {

		@Override
		public void onLocationChanged(Location l) {
			float accuracy = l.getAccuracy();
			double latitude = l.getLatitude();
			double longitude = l.getLongitude();
			String provider = l.getProvider();
			Log.i("RoomActivity.onLocationChanged", "Lat=" + latitude + " Lon=" + longitude + " Accuracy=" + accuracy
					+ " Provider: " + provider);
			
			try {
				lock.lock();
				
				location = l;
				
			} finally {
				lock.unlock();
			}
			
			updateRoomList();
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			String statusName = "";
			if (status == LocationProvider.OUT_OF_SERVICE)
				statusName = "OUT_OF_SERVICE";
			if (status == LocationProvider.TEMPORARILY_UNAVAILABLE)
				statusName = "TEMPORARILY_UNAVAILABLE";
			if (status == LocationProvider.AVAILABLE)
				statusName = "AVAILABLE";
			
			int satellites = extras.getInt("satellites");
			
			Log.d("RoomActivity.onStatusChanged", "New status: " + statusName + "; Satellites: " + satellites);
		}

		@Override
		public void onProviderEnabled(String provider) {
			Log.d("RoomActivity.onProviderEnabled", "New provider: " + provider);
		}

		@Override
		public void onProviderDisabled(String provider) {
			Log.d("RoomActivity.onProviderDisabled", "Disabled provider: " + provider);
		}
		
	}
	
	class RoomsUpdaterCoarse implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}
	}
	
	class CreateRoomListener implements DialogInterface.OnClickListener {
		Location l;
		
		public CreateRoomListener(Location l) {
			this.l = l;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			EditText nameView = (EditText) ((AlertDialog) dialog).findViewById(R.id.newRoomName);
			JSONObject j = new JSONObject();

			final String roomName = nameView.getText().toString();
			
			if (roomName == null || roomName.length() == 0) {
				Toast.makeText(RoomActivity.this, getString(R.string.toast_room_name_empty), Toast.LENGTH_SHORT).show();
				return;
			}
			
			try {
				
				double latitude = l.getLatitude();
				double longitude = l.getLongitude();
				
				// Max 6 decilam digits are supported by the REST server
				latitude = (double)((int)(latitude * 1000000)) / 1000000;
				longitude = (double)((int)(longitude * 1000000)) / 1000000;
				
				j.put("latitude", latitude);
				j.put("longitude", longitude);
				j.put("roomName", roomName);
				
			} catch (JSONException e) {
				Log.e("RoomActivity.CreateRoomListener", "JSONException while creating a new room. Error message: " + e.getMessage());
				Toast.makeText(RoomActivity.this, getString(R.string.toast_room_creation_failed), Toast.LENGTH_SHORT).show();
				return;
			}
			
			new AsyncTask<String, Void, String>() {

				@Override
				protected String doInBackground(String... params) {
					String message = params[0];
					String response;
					
					REST rest = REST.getInstance();
					try {
						response = rest.createRoom(message);
					} catch (RESTException e) {
						Log.e("RoomActivity.CreateRoomListener", "RESTException while creating a new room. Error message: " + e.getMessage());
						return null;
					}
					
					return response;
				}
				
				protected void onPostExecute(String result) {
					if (result == null) {
						Toast.makeText(RoomActivity.this, getString(R.string.toast_room_creation_failed), Toast.LENGTH_SHORT).show();
						return;
					}
					
					try {
						JSONObject j = new JSONObject(result);
						RoomEntry room = new RoomEntry(j.getString("roomID"), roomName, 0, 0, j.getString("stream_url"));
						
						// Start new activity
						Intent intent = new Intent(RoomActivity.this, RecordingActivity.class);
						intent.putExtra(SplashScreen.SPLASH_NAME, username);
						intent.putExtra(SplashScreen.SPLASH_TOKEN, token);
						intent.putExtra(ROOM_OBJECT, room);
						
						startActivity(intent);
						
					} catch (JSONException e) {
						Log.e("RoomActivity.CreateRoomListener", "JSONException while creating a new room. Error message: " + e.getMessage());
						Toast.makeText(RoomActivity.this, getString(R.string.toast_room_creation_failed), Toast.LENGTH_SHORT).show();
					}
					
				};
				
			}.execute(j.toString());
			
		}
		
	}
}
