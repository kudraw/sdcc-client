package it.uniroma2.sdcc.gui;

import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.RoomEntry;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RoomsAdapter extends BaseAdapter {
	
	private List<RoomEntry> roomList;
	
	private final Context context;
	
	public RoomsAdapter(Context context) {
		this.context = context;
		this.roomList = new ArrayList<RoomEntry>();
	}
	
	public void add(RoomEntry entry) {
		roomList.add(entry);
		
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return roomList.size();
	}

	@Override
	public RoomEntry getItem(int pos) {
		return roomList.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
	        convertView = LayoutInflater.from(context).inflate(R.layout.room_adapter, parent, false);
	    }

	    TextView viewTitle = (TextView) convertView.findViewById(R.id.room_adapter_title);
	    TextView viewParticipants = (TextView) convertView.findViewById(R.id.room_adapter_participants_value);
	    TextView viewDistance = (TextView) convertView.findViewById(R.id.room_adapter_distance_value);

	    RoomEntry entry = roomList.get(pos);
	    
	    if (entry.getName() != null && entry.getName().length() > 0)
	    	viewTitle.setText(entry.getName());
	    viewParticipants.setText(String.valueOf(entry.getParticipants()));
	    viewDistance.setText(String.valueOf(entry.getDistance()) + " m");

	    return convertView;
	}

}
