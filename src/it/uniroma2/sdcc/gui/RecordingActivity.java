package it.uniroma2.sdcc.gui;

import java.util.Locale;

import it.uniroma2.sdcc.NetworkThread;
import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.RecorderService;
import it.uniroma2.sdcc.StatsThread;
import it.uniroma2.sdcc.RecorderService.RecorderBinder;
import it.uniroma2.sdcc.RoomEntry;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

public class RecordingActivity extends Activity {
	public final String TAG = "RecordingActivity";

	private String username;
	private String token;
	private RoomEntry room;
	private RecorderService service;

	private Intent recorderIntent;

	private Handler handler;

	private Thread stats;

	private TextView textStatus;

	private ServiceConnection srvConn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			service = null;

			Log.d(TAG, "Service disconnected.");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder bindr) {
			RecorderBinder binder = (RecorderBinder) bindr;

			service = binder.getService();

			Log.d(TAG, "Service connected.");

			if (!service.isStarted()) {
				Log.i(TAG, "Service starting...");
				service.start();
			}

			/*runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// consistently restore state

					if (service.isStarted())
						buttonStop.setEnabled(true);
					else
						buttonStart.setEnabled(true);
				}
			});*/
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recording_activity);
		
		Log.d(TAG+".onCreate", "Activity created");
		
		textStatus = (TextView) findViewById(R.id.textStatus);

		Bundle extras = getIntent().getExtras();

		if (extras == null) {
			// Something went wrong
			finish();
		}

		service = null;

		recorderIntent = new Intent(this, RecorderService.class);

		recorderIntent.putExtras(extras);

		username = extras.getString(SplashScreen.SPLASH_NAME);
		token = extras.getString(SplashScreen.SPLASH_TOKEN);
		room = (RoomEntry) extras.getSerializable(RoomActivity.ROOM_OBJECT);
		
		Log.i(TAG+".onCreate", "RoomID: " + room.getID() + " Stream URL: " + room.getUrl());

		handler = new Handler( new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				String status;
				if (msg.arg1 == 0)
					status = "Connection problems";
				else {
					double rate = msg.arg2;
					String unit = "b/s";

					if (msg.arg2 > 1024) {
						rate = msg.arg2 / 1024.0;
						unit = "kb/s";
					}

					status = String.format(Locale.ENGLISH, "Net stats: %.2f %s", rate, unit);
				}

				textStatus.setText(status);

				return true;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();

		if (!bindService(recorderIntent, srvConn, 0)) {
			Log.e(TAG, "Binding failed");
		}

		startService(recorderIntent);

		stats = new StatsThread(handler);
		stats.start();
	}

	@Override
	public void onBackPressed() {

		if (service != null) {

			if (service.isStarted()) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(R.string.recording_alert_stop_message)
						.setTitle(R.string.recording_alert_stop_title)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								service.stop();
								RecordingActivity.super.onBackPressed();
							}

						})
						.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
							}
				}).create().show();
			}
		}
	}

	@Override
	protected void onStop() {

		stats.interrupt();

		if (service != null) {

			if (!service.isStarted()) {
				stopService(recorderIntent);
			}

			unbindService(srvConn);

			Log.d(TAG, "Service unbind");
			service = null;
		}
		
		super.onStop();
	}
}
