package it.uniroma2.sdcc.gui;

import it.uniroma2.sdcc.FileEntry;
import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.nethelpers.REST;
import it.uniroma2.sdcc.nethelpers.RESTException;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String username = null;
	private String token = null;
	
	private ListView filesList;
	private TextView noUserView;
	private TextView emptyList;
	private ProgressBar waitBar;
	private FilesAdapter filesAdapter;

	/*private RecorderService service;

	private Intent recorderIntent;

	private ServiceConnection srvConn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			service = null;

			Log.i("MainActivity", "Service disconnected.");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder bindr) {
			RecorderBinder binder = (RecorderBinder) bindr;

			service = binder.getService();

			Log.i("MainActivity", "Service connected.");

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// consistently restore state

					if (service.isStarted())
						buttonStop.setEnabled(true);
					else
						buttonStart.setEnabled(true);
				}
			});
		}
	};*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Log.d("MainActivity.onCreate", "Activity created");

		filesList = (ListView) findViewById(R.id.list);
		noUserView = (TextView) findViewById(R.id.no_user_message);
		emptyList = (TextView) findViewById(R.id.empty_list_message);
		waitBar = (ProgressBar) findViewById(R.id.wait_for_file_list);
		
		/*buttonStart = (Button) findViewById(R.id.buttonStart);
		buttonStop = (Button) findViewById(R.id.buttonStop);

		service = null;

		recorderIntent = new Intent(this, RecorderService.class);*/

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			username = extras.getString(SplashScreen.SPLASH_NAME);
			token = extras.getString(SplashScreen.SPLASH_TOKEN);
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		
		Log.d("MainActivity.onStart", "Activity started");
		
		// Delete handler for elements in the list
		filesList.setOnItemLongClickListener(new OnItemLongClickListener () {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				FileEntry f = filesAdapter.getItem(position);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setMessage(R.string.main_alert_delete_message)
						.setTitle(R.string.main_alert_delete_title)
						.setPositiveButton(android.R.string.ok, new DeleteDialog(f))
						.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {	
							}
				}).create().show();
				
				return true;
			}
			
		});
		
		filesList.setOnItemClickListener(new OnItemClickListener () {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				FileEntry f = filesAdapter.getItem(position);
				String url = f.getUrl();
				
				if (url == null || url.length() == 0 || url.equalsIgnoreCase("null")) {
					Toast.makeText(MainActivity.this, getString(R.string.toast_file_not_found), Toast.LENGTH_SHORT).show();
				}
				else {
					
					Log.d("MainActivity.onStart", "Opening browser intent for URL: " + url);
					
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(browserIntent);
				}
				
			}
			
		});

		if (username != null && token != null) {
			Log.i("MainActivity.onCreate", "Application started with user " + username);
			updateFileList();
		}
		else {
			Log.i("MainActivity.onCreate", "Application started without any user");
			filesList.setVisibility(View.INVISIBLE);
			noUserView.setVisibility(View.VISIBLE);
		}

		/*if (!bindService(recorderIntent, srvConn, 0)) {
			Log.e("MainActivity", "Binding failed");
		}

		startService(recorderIntent);*/
	}
	
	private void updateFileList() {
		emptyList.setVisibility(View.INVISIBLE);
		filesList.setVisibility(View.INVISIBLE);
		waitBar.setVisibility(View.VISIBLE);
		
		Log.d("MainActivity.updateFileList", "Updating list view");
		
		filesAdapter = new FilesAdapter(this);
		filesList.setAdapter(filesAdapter);
		
		new AsyncTask<Void, JSONObject, Boolean>() {

			@Override
			protected Boolean doInBackground(Void... params) {
				REST rest = REST.getInstance();
				
				try {
					String response = rest.getUserFileList();
					
					JSONArray res = new JSONArray(response);
					
					for(int i = 0; i < res.length(); i++) {
						JSONObject o = res.getJSONObject(i);
						publishProgress(o);
					}
					
					return true;
					
				} catch (RESTException | JSONException e) {
					Log.e("MainActivity.updateFileList.doInBackground", "Error: " + e.getMessage());
				}
				
				return false;
			}
			
			protected void onProgressUpdate(JSONObject... values) {
				JSONObject o = values[0];
				
				try {
					
					// Get human readable value for file dimension
					int dim = o.getInt("dimension");
					String unit = "bytes";
					while (dim / 1024 > 0) {
						dim /= 1024;
						if (unit.equals("bytes"))
							unit = "KB";
						else if (unit.equals("KB"))
							unit = "MB";
						else if (unit.equals("MB")) {
							unit = "TB";
							break;
						}
					}
					String dimension = dim + " " + unit;
					
					double minutes = o.getDouble("length");
					int seconds = (int)((minutes - (int)minutes) * 1000) * 60 / 1000;
					String length = String.format(Locale.ITALY, "%02d:%02d min", (int)minutes, seconds);
					
					FileEntry entry = new FileEntry(o.getString("recordingID"), o.getString("recordingName"),
							length, dimension, o.getString("download_url"),
							o.getString("start_recording"), (float)o.getDouble("latitude"), (float)o.getDouble("longitude"));
					
					filesAdapter.add(entry);
				} catch (JSONException e) {
					Log.e("MainActivity.updateFileList.onProgressUpdate", "JSONException: " + e.getMessage());
				}
			};
			
			protected void onPostExecute(Boolean result) {
				if (result == true) {
					waitBar.setVisibility(View.INVISIBLE);
					if (filesAdapter.isEmpty())
						emptyList.setVisibility(View.VISIBLE);
					else
						filesList.setVisibility(View.VISIBLE);
				}
				else {
					waitBar.setVisibility(View.INVISIBLE);
					Toast.makeText(MainActivity.this, getString(R.string.toast_server_error), Toast.LENGTH_SHORT).show();
				}
			};
		}.execute();
	}

	/*public void onButtonStart(View v) {

		v.setEnabled(false);

		service.start();

		buttonStop.setEnabled(true);
	}

	public void onButtonStop(View v) {

		v.setEnabled(false);

		service.stop();

		buttonStart.setEnabled(true);
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		Log.d("MainActivity", "Action selected");
		
		int id = item.getItemId();
		
		if (id == R.id.action_update_files) {
			if (username != null) {
				updateFileList();
				return true;
			}
			else {
				Toast.makeText(this, getString(R.string.toast_login_first), Toast.LENGTH_SHORT).show();
				return false;
			}
		}
		else if (id == R.id.action_record) {
			if (username != null) {

				Intent intent = new Intent(MainActivity.this, RoomActivity.class);
				intent.putExtra(SplashScreen.SPLASH_NAME, username);
				intent.putExtra(SplashScreen.SPLASH_TOKEN, token);
				
				startActivity(intent);
				
				return true;
			}
			else {
				Toast.makeText(this, getString(R.string.toast_login_first), Toast.LENGTH_SHORT).show();
				return false;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {

		/*if (service != null) {

			if (!service.isStarted()) {
				stopService(recorderIntent);
			}

			unbindService(srvConn);

			Log.i("MainActivity", "Service unbind");
			service = null;
		}*/

		super.onStop();
	}
	
	/**
	 * 
	 * OnClickListener for OK button of the delete dialog box
	 *
	 */
	class DeleteDialog implements DialogInterface.OnClickListener {
		FileEntry entry;
		
		public DeleteDialog(FileEntry entry) {
			if (entry == null)
				throw new NullPointerException("FileEntry argument cannot be null");
			this.entry = entry;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			String fileID = entry.getID();
			
			filesList.setVisibility(View.INVISIBLE);
			waitBar.setVisibility(View.VISIBLE);
			
			new AsyncTask<String, Void, Boolean>() {

				@Override
				protected Boolean doInBackground(String... params) {
					REST rest = REST.getInstance();
					String recordingID = params[0];
					
					try {
						rest.deleteFile(recordingID);
						return true;
					} catch (RESTException e) {
						Log.e("MainActivity.DeleteDialog", "Failed to delete recording. Error message: " + e.getMessage());
					}
					
					return false;
				}
				
				protected void onPostExecute(Boolean result) {
					if (result == true)
						Toast.makeText(MainActivity.this, getString(R.string.toast_file_deleted), Toast.LENGTH_SHORT).show();
					else
						Toast.makeText(MainActivity.this, getString(R.string.toast_failed), Toast.LENGTH_SHORT).show();
					updateFileList();
				};
				
			}.execute(fileID);
			
		}
	}
}
