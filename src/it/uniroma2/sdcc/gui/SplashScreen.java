package it.uniroma2.sdcc.gui;

import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.authenticator.AuthenticatorActivity;
import it.uniroma2.sdcc.nethelpers.REST;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class SplashScreen extends Activity {
	public static final String SPLASH_NAME = "NAME";
	public static final String SPLASH_TOKEN = "TOKEN";
	
	AccountManager manager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		manager = AccountManager.get(this);
		
		Account[] accts = manager.getAccountsByType(AuthenticatorActivity.ACCOUNT_TYPE);
		
		if (accts.length == 0) {
			Log.d("SplashScreen.onCreate", "No account found, starting intent for creating a new one");
			
			Intent intent = new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT);
			intent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[] {AuthenticatorActivity.ACCOUNT_TYPE});
			startActivityForResult(intent, 1);
			
		}
		else {
			Log.d("SplashScreen.onCreate", "Authenticating user");
			getAccountAndClose(accts[0]);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				Log.d("SplashScreen.onActivityResult", "Account creation completed correctly");
				
				Account[] accts = manager.getAccountsByType(AuthenticatorActivity.ACCOUNT_TYPE);
				if (accts.length == 0) {
					Log.e("SplashScreen.onActivityResult", "Account creation successful, but can't get back new account");
					getAccountAndClose(null);
				}
				else {
					getAccountAndClose(accts[0]);
				}
				
			}
			else if (resultCode == RESULT_CANCELED) {
				Log.w("SplashScreen.onActivityResult", "Account creation aborted");
				startActivity(new Intent(this, MainActivity.class));
				finish();
			}
			
		}
	}
	
	private void getAccountAndClose(Account account) {
		
		if (account != null) {
			manager.getAuthToken(account, AuthenticatorActivity.ARG_AUTH_TYPE, null, this, new AccountManagerCallback<Bundle>() {
				
				@Override
				public void run(AccountManagerFuture<Bundle> future) {
					try {
						Bundle authTokenBundle = future.getResult();
						String authToken = authTokenBundle.get(AccountManager.KEY_AUTHTOKEN).toString();
						String username = authTokenBundle.get(AccountManager.KEY_ACCOUNT_NAME).toString();
	
						Log.i("SplashScreen.getAccountAndClose", "Logged as " + username + ". AuthToken: " + authToken);
						
						// Invalidate this token for next login (force the recheck of the token) 
						AccountManager.get(SplashScreen.this).invalidateAuthToken(AuthenticatorActivity.ARG_ACCOUNT_TYPE, authToken);
	
						// Jump to MainActivity
						Intent main = new Intent(SplashScreen.this, MainActivity.class);
						main.putExtra(SPLASH_NAME, username);
						main.putExtra(SPLASH_TOKEN, authToken);
						
						REST rest = REST.getInstance();
						rest.setToken(authToken);
						
						startActivity(main);
						finish();
						
					} catch (OperationCanceledException | AuthenticatorException
							| IOException e) {
						Log.e("SplashScreen.getAccountAndClose", "Error getting account informations: " + e.getMessage());
						
						startActivity(new Intent(SplashScreen.this, MainActivity.class));
						finish();
					}
					
				}
			}, null);
		}
		else {
			
			startActivity(new Intent(SplashScreen.this, MainActivity.class));
			finish();
			
		}
		
	}
}
