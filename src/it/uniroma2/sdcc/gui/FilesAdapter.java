package it.uniroma2.sdcc.gui;

import it.uniroma2.sdcc.FileEntry;
import it.uniroma2.sdcc.R;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FilesAdapter extends BaseAdapter {
	
	private List<FileEntry> fileList;
	
	private final Context context;
	
	public FilesAdapter(Context context) {
		this.context = context;
		this.fileList = new ArrayList<FileEntry>();
	}
	
	public void add(FileEntry entry) {
		fileList.add(entry);
		
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return fileList.size();
	}

	@Override
	public FileEntry getItem(int pos) {
		return fileList.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
	        convertView = LayoutInflater.from(context).inflate(R.layout.file_adapter, parent, false);
	    }

	    TextView viewTitle = (TextView) convertView.findViewById(R.id.room_adapter_title);
	    TextView viewDate = (TextView) convertView.findViewById(R.id.room_adapter_participants_value);
	    TextView viewDuration = (TextView) convertView.findViewById(R.id.file_adapter_duration);
	    TextView viewDimension = (TextView) convertView.findViewById(R.id.room_adapter_distance_value);

	    FileEntry entry = fileList.get(pos);
	    
	    if (entry.getName() != null && entry.getName().length() > 0)
	    	viewTitle.setText(entry.getName());
	    viewDate.setText(entry.getDate());
	    viewDuration.setText(entry.getDuration());
	    viewDimension.setText(entry.getDimension());

	    return convertView;
	}

}
