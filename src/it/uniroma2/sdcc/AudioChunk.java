package it.uniroma2.sdcc;

public class AudioChunk {
	
	private byte[] buffer;
	private int size;

	public AudioChunk(byte[] buffer, int size) {
		this.buffer = buffer;
		this.size = size;
	}
	
	public byte[] getBuffer() {
		return buffer;
	}
	
	public int getSize() {
		return size;
	}
}
