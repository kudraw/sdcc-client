package it.uniroma2.sdcc.authenticator;

public class AuthenticationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7171835412948731862L;
	
	public AuthenticationException(String m) {
		super(m);
	}
	
}
