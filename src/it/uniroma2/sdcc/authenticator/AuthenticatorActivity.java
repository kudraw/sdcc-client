package it.uniroma2.sdcc.authenticator;

import it.uniroma2.sdcc.R;
import it.uniroma2.sdcc.nethelpers.NetAuthenticatorConnector;

import java.io.IOException;

import org.json.JSONException;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class AuthenticatorActivity extends AccountAuthenticatorActivity {
	public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
	public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
	public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
	public final static String ARG_IS_ADDING_NEW_ACCOUNT = "NEW_ACCOUNT";
	public final static String PARAM_USER_PASS = "PASSWORD";
	public static final String KEY_ERROR_MESSAGE = "ERR_MSG";
	
	public final static String ACCOUNT_TYPE = "it.uniroma2.sdcc";
	public final static String AUTHTOKEN_TYPE = "client token";
	
	private AccountManager mAccountManager;
	private String mAuthTokenType;

	private TextView textViewUsername;
	private TextView textViewPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.d("AuthenticatorActivity.onCreate", "Started activity");
		
		setContentView(R.layout.authenticator_login);
		mAccountManager = AccountManager.get(getBaseContext());

		textViewUsername = (TextView)findViewById(R.id.login_username_box);
		textViewPassword = (TextView)findViewById(R.id.login_password_box);

		String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
		mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);

		if (mAuthTokenType == null)
			mAuthTokenType = AUTHTOKEN_TYPE;

		if (accountName != null) {
			textViewUsername.setText(accountName);
		}

	}

	public void submit(View view) {
		final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
		final String username = textViewUsername.getText().toString();
		final String password = textViewPassword.getText().toString();

		new AsyncTask<String, Void, Intent>() {

			@Override
			protected Intent doInBackground(String... params) {
				Log.d("AuthenticatorActivity.submit", "Started authentication procedure");

				String authtoken = null;
				Bundle data = new Bundle();
				try {
					authtoken = NetAuthenticatorConnector.userSignIn(username, password);

					data.putString(AccountManager.KEY_ACCOUNT_NAME, username);
					data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
					data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
					data.putString(PARAM_USER_PASS, password);
				} catch (AuthenticationException | IOException | JSONException e) {
					data.putString(KEY_ERROR_MESSAGE, e.getMessage());
				}

				final Intent res = new Intent();
				res.putExtras(data);
				return res;
			}

			protected void onPostExecute(Intent result) {
				if (result.hasExtra(KEY_ERROR_MESSAGE)) {
					Toast.makeText(getBaseContext(), result.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
				} else {
					Log.d("AuthenticatorActivity.finishLogin", "Login successful, saving credentials");
					finishLogin(result);
				}
			}
		}.execute();
	}

	private void finishLogin(Intent intent) {
		String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
		String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
		final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

		if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
			Log.d("AuthenticatorActivity.finishLogin", "Adding a new account explicitly");

			String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
			String authtokenType = mAuthTokenType;

			// Creating the account on the device and setting the auth token we got
			// (Not setting the auth token will cause another call to the server to authenticate the user)
			mAccountManager.addAccountExplicitly(account, accountPassword, null);
			mAccountManager.setAuthToken(account, authtokenType, authtoken);
		} else {
			Log.d("AuthenticatorActivity.finishLogin", "Editing an existing account");
			mAccountManager.setPassword(account, accountPassword);
		}

		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		setResult(RESULT_CANCELED);
		finish();
	}
}
