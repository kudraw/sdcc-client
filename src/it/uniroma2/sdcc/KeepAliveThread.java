package it.uniroma2.sdcc;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import android.util.Log;

public class KeepAliveThread extends Thread {
	
	public static final String TAG = "KeepAliveThread";

	Socket sock;
	int timeout;
	
	public KeepAliveThread(Socket sock, int timeout) {
		this.sock = sock;
		this.timeout = timeout;
	}
	
	@Override
	public void run() {
		
		Log.i(TAG, "Keep Alive Thread started");
		
		try {
			sock.setSoTimeout(timeout);
			
			while ( sock.getInputStream().read() != -1 )
				;
			
			sock.close();
			
			Log.i(TAG, "Server close connection");
		} catch (SocketException e) {
			Log.e(TAG, e.getMessage());
		} catch (SocketTimeoutException e) {
			Log.i(TAG, "Server timeout");
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} 
	}
}
