package it.uniroma2.sdcc;

import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.util.Log;

/**
 * The EncoderInput class wraps the Android APIs, providing a single 
 * put function to offer data to encoder routine.
 * 
 * Note that this class DOES NOT encode data, just provides a convenient
 * interface for data input.
 *  
 * @author CoDeP Team
 *
 */
public class EncoderInput {

	public static final String TAG = "EncoderInput";
	
	private CodecInfo info;
	
	/**
	 * EncoderInput constructor.
	 * 
	 * @param info - A CodecInfo instance containing codec information.
	 */
	public EncoderInput(CodecInfo info) {
		this.info = info;
	}
	
	/**
	 * The put function will forward raw audio data to Android MediaCodec Encoder.
	 * 
	 * @param input - The raw PCM stream buffer, passing a null pointer will result in End-Of-Stream packet emission.
	 * @param len - Length of data in the buffer
	 */
	public void put(byte[] input, int len) {
		int flags = 0;

        int inputBufferIndex = info.codec.dequeueInputBuffer(0); // timeout in us
        
        if (inputBufferIndex < 0) {
        	
        	Log.w(TAG, "Unable to dequeue input buffer");
        	return;
        }
        
        ByteBuffer inputBuffer = info.inputBuffers[inputBufferIndex];
        inputBuffer.clear();
        
        if (input != null) {
        	// Normal flow
        	
        	inputBuffer.put(input, 0, len);
        }
        else {
        	Log.i(TAG, "End Of Stream");
        	
        	len = 0;
        	
        	flags |= MediaCodec.BUFFER_FLAG_END_OF_STREAM;
        }
        
        info.codec.queueInputBuffer(inputBufferIndex, 0, len, 0, flags);
	}
}
