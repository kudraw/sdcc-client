package it.uniroma2.sdcc;

import java.util.Queue;

public interface Encoder {

	public void encode(byte[] input, int len, Queue<AudioChunk> outputQueue);
}
