package it.uniroma2.sdcc;

import java.io.Serializable;

public class RoomEntry implements Serializable {
	
	private static final long serialVersionUID = -8698210289874999139L;
	
	String roomID;
	String name;
	int distance; // meters
	int participants;
	String url;
	
	public RoomEntry(String roomID, String name, int distance,
			int participants, String url) {
		if (roomID == null) {
			throw new NullPointerException("On RoomEntry construction, null arguments.");
		}
		
		this.roomID = roomID;
		this.name = name;
		this.distance = distance;
		this.participants = participants;
		this.url = url;
	}
	
	public String getID() {
		return roomID;
	}

	public String getName() {
		return name;
	}

	public int getDistance() {
		return distance;
	}
	
	public int getParticipants() {
		return participants;
	}

	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}
